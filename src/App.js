import { useState, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";

import Header from "./components/header";
import ProductList from "./components/productList";
import LogInModal from "./components/loginModal";
import SignupModal from "./components/signupModal";
import CartModal from "./components/cartModal";

import {
  fetchProducts,
  fetchCart,
  updateCart,
  checkoutCart,
  fetchUser,
} from "./actions";
import { USER_TOKEN } from "./constants";
import { createCookie, readCookie, eraseCookie } from "./utils";

import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import "react-toastify/dist/ReactToastify.css";

function App() {
  const [currentUser, setCurrentUser] = useState(null);

  const [cart, setCart] = useState([]);
  const [checkoutLoading, setCheckoutLoading] = useState(false);

  const [products, setProducts] = useState([]);
  const [productsLoading, setProductsLoading] = useState(false);

  const [productIdLoading, setProductIdLoading] = useState("");

  const [loginModalVisible, setLoginModalVisible] = useState(false);
  const [signupModalVisible, setSignupModalVisible] = useState(false);
  const [cartModalVisible, setCartModalVisible] = useState(false);

  useEffect(() => {
    const fetchUserData = async (userId) => {
      const res = await fetchUser(userId);

      if (res.data) {
        setCurrentUser(res.data);
      } else {
        eraseCookie(USER_TOKEN);
      }
    };

    const token = readCookie(USER_TOKEN);
    if (token) {
      fetchUserData(token);
    }
  }, []);

  useEffect(() => {
    const fetchProductsData = async () => {
      setProductsLoading(true);
      try {
        const res = await fetchProducts();
        setProducts(res.data);
      } catch (err) {
        console.error(err);
      } finally {
        setProductsLoading(false);
      }
    };

    fetchProductsData();
  }, []);

  useEffect(() => {
    const fetchCartData = async () => {
      try {
        const { token: userId } = currentUser;
        const res = await fetchCart(userId);
        setCart(res.data.items);
      } catch (err) {
        console.error(err);
      }
    };

    if (currentUser) {
      fetchCartData();
    }
  }, [currentUser]);

  const onToggleLoginModal = () => {
    setLoginModalVisible((prev) => !prev);
  };

  const onToggleSignupModal = () => {
    setSignupModalVisible((prev) => !prev);
  };

  const onToggleCartModal = () => {
    setCartModalVisible((prev) => !prev);
  };

  const clearUser = () => {
    setCurrentUser("");
    setCart([]);
    eraseCookie(USER_TOKEN);
  };

  const setUser = (user) => {
    setCurrentUser(user);
    createCookie(USER_TOKEN, user.token);
  };

  const addToCart = (idProduct) => async () => {
    if (currentUser) {
      let product = products.find((prod) => prod.id === idProduct);
      product = {
        productId: product.id,
        name: product.name,
        price: product.price,
        quantity: 1,
      };
      if (product) {
        setProductIdLoading(product.id);
        try {
          const { token: userId } = currentUser;
          const res = await updateCart(userId, product);
          setCart(res.data.items);
          toast.success("🚀 Add to cart successfully!");
        } catch (err) {
          console.error(err);
        } finally {
          setProductIdLoading("");
        }
      }
    } else {
      setLoginModalVisible(true);
    }
  };

  const checkout = async () => {
    if (cart.length === 0) {
      return;
    }

    setCheckoutLoading(true);
    try {
      const { token: userId } = currentUser;
      const total = cart.reduce(
        (t, { price, quantity }) => t + price * quantity,
        0
      );

      await checkoutCart(userId, {
        userId,
        total,
        products: cart,
      });

      setCart([]);
      toast.success("🚀 Checkout your cart successfully!");
    } catch (err) {
      console.error(err);
    } finally {
      setCheckoutLoading(false);
    }
  };

  return (
    <div className="App">
      <Header
        currentUser={currentUser?.name}
        clearUser={clearUser}
        setUser={setUser}
        addToCart={addToCart}
        onToggleLoginModal={onToggleLoginModal}
        onToggleSignupModal={onToggleSignupModal}
        onToggleCartModal={onToggleCartModal}
      />
      {productsLoading && <div>Loading...</div>}
      {!productsLoading && (
        <ProductList
          products={products}
          productIdLoading={productIdLoading}
          addToCart={addToCart}
        />
      )}
      <LogInModal
        setUser={setUser}
        isOpen={loginModalVisible}
        onToggle={onToggleLoginModal}
      />
      <SignupModal isOpen={signupModalVisible} onToggle={onToggleSignupModal} />
      <CartModal
        cartTable={cart}
        isOpen={cartModalVisible}
        onToggle={onToggleCartModal}
        onCheckout={checkout}
        checkoutLoading={checkoutLoading}
      />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}

export default App;
