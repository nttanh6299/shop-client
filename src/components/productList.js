import React from "react";
import { CardGroup } from "reactstrap";
import ProductCard from "./productCard";

const ProductList = ({ addToCart, productIdLoading, products = [] }) => {
  if (products.length === 0) {
    return null;
  }

  return (
    <CardGroup>
      {products.map((product) => (
        <ProductCard
          loading={product.id === productIdLoading}
          key={product.id}
          addToCart={addToCart}
          {...product}
        />
      ))}
    </CardGroup>
  );
};

export default ProductList;
