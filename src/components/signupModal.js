import React, { useState } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Input,
} from "reactstrap";
import { signup } from "../actions";
import { toast } from "react-toastify";

const SignupModal = ({ isOpen, onToggle }) => {
  const [formData, setFormData] = useState({});
  const [loading, setLoading] = useState(false);

  const onTextChange = (e) => {
    const { name, value } = e.target;
    setFormData((prev) => ({ ...prev, [name]: value }));
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    try {
      await signup(formData);
      toast.success("Sign up successfully");
      onToggle();
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Modal isOpen={isOpen} toggle={onToggle}>
      <ModalHeader toggle={onToggle}>Signup</ModalHeader>
      <ModalBody>
        <Form onSubmit={onSubmit}>
          <FormGroup>
            <Input
              onChange={onTextChange}
              type="text"
              name="name"
              placeholder="Name"
              required
            />
          </FormGroup>{" "}
          <FormGroup>
            <Input
              onChange={onTextChange}
              type="email"
              name="email"
              placeholder="Email"
              required
            />
          </FormGroup>{" "}
          <FormGroup>
            <Input
              onChange={onTextChange}
              type="password"
              name="password"
              placeholder="Password"
              required
            />
          </FormGroup>{" "}
          <Button disabled={loading} type="submit" color="success">
            Submit
          </Button>
        </Form>
      </ModalBody>
    </Modal>
  );
};

export default SignupModal;
