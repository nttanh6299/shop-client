import React from "react";
import { Card, Button, CardTitle, CardText } from "reactstrap";

const ProductCard = ({ id, name, description, price, addToCart, loading }) => {
  return (
    <Card body>
      <CardTitle tag="h4">{name}</CardTitle>
      <CardTitle tag="h5">${price}</CardTitle>
      <CardText>{description}</CardText>
      <Button onClick={addToCart(id)} disabled={loading} color="success">
        {loading ? "Loading..." : "Add to cart"}
      </Button>
    </Card>
  );
};

export default ProductCard;
