import React from "react";
import { Navbar, NavbarBrand, Nav, NavItem } from "reactstrap";

const Header = ({
  currentUser,
  clearUser,
  onToggleLoginModal,
  onToggleSignupModal,
  onToggleCartModal,
}) => {
  return (
    <>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">
          <h1>Shoppeew</h1>
        </NavbarBrand>
        <Nav>
          {!currentUser && (
            <>
              <NavItem onClick={onToggleLoginModal}>
                <span style={{ cursor: "pointer" }}>Login &nbsp;&nbsp;</span>
              </NavItem>
              <NavItem onClick={onToggleSignupModal}>
                <span style={{ cursor: "pointer" }}>Sign up</span>
              </NavItem>
            </>
          )}
          {currentUser && (
            <>
              <NavItem>
                <span style={{ cursor: "pointer", fontWeight: "700" }}>
                  {currentUser} &nbsp;&nbsp;
                </span>
              </NavItem>
              <NavItem onClick={onToggleCartModal}>
                <span style={{ cursor: "pointer", color: "green" }}>
                  Cart &nbsp;&nbsp;
                </span>
              </NavItem>
              <NavItem onClick={clearUser}>
                <span style={{ cursor: "pointer", color: "red" }}>Logout</span>
              </NavItem>
            </>
          )}
        </Nav>
      </Navbar>
    </>
  );
};

export default Header;
