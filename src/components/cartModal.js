import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Table,
} from "reactstrap";

const CartModal = ({
  isOpen,
  onToggle,
  cartTable,
  onCheckout,
  checkoutLoading,
}) => {
  const totalPrice = cartTable.reduce(
    (total, data) => total + data.price * data.quantity,
    0
  );

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={!checkoutLoading ? onToggle : () => {}}>
        Your cart
      </ModalHeader>
      <ModalBody>
        <Table bordered>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            {cartTable.map((data, index) => (
              <tr key={index}>
                <th scope="row">{index + 1}</th>
                <td>{data.name}</td>
                <td>{data.price}</td>
                <td>{data.quantity}</td>
                <td>{data.price * data.quantity}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <div style={{ textAlign: "right" }}>Total: ${totalPrice}</div>
      </ModalBody>
      <ModalFooter>
        <Button
          disabled={checkoutLoading}
          color="success"
          onClick={cartTable.length > 0 ? onCheckout : null}
        >
          {checkoutLoading ? "Loading..." : "Checkout"}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default CartModal;
