import { api } from "../api";

export const fetchProducts = () => {
  return api.get("/products");
};

export const fetchCart = (userId) => {
  return api.get(`/shopping-carts/${userId}`);
};

export const updateCart = (userId, product) => {
  return api.post(`/shopping-carts/${userId}/add`, product);
};

export const checkoutCart = (userId, cart) => {
  return api.post(`/users/${userId}/orders`, cart);
};

export const login = (credential) => {
  return api.post("/users/login", credential);
};

export const signup = (user) => {
  return api.post("/users", user);
};

export const fetchUser = (userId) => {
  return api.get("/users/" + userId);
};
